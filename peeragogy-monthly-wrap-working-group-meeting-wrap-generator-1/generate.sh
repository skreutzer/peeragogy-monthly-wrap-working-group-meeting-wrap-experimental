#!/bin/sh

mkdir -p ./html/
mkdir -p ./epub/temp/
mkdir -p ./latex_1/
mkdir -p ./latex_2/


for filepath in ./PeeragogyMonthlyWrap/meeting-wraps/*.md
do
    # Only file name.
    file=${filepath##*/}
    filename=${file%.*}

    # Or "-f gfm" in case of a more recent Pandoc package.
    pandoc --email-obfuscation=none -f markdown_github-autolink_bare_uris $filepath -o $filepath.html

    mv $filepath.html ./html/$filename.html
done


printf "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" > ./jobfile_text_concatenator_1.xml
printf "<text-concatenator-1-job>\n" >> ./jobfile_text_concatenator_1.xml
printf "  <input>\n" >> ./jobfile_text_concatenator_1.xml

for filepath in ./html/*
do
    printf "    <text-file path=\"%s\"/>\n" $filepath >> ./jobfile_text_concatenator_1.xml
done

printf "  </input>\n" >> ./jobfile_text_concatenator_1.xml
printf "  <output-file path=\"./html/all.html\"/>\n" >> ./jobfile_text_concatenator_1.xml
printf "</text-concatenator-1-job>\n" >> ./jobfile_text_concatenator_1.xml

java -cp ./digital_publishing_workflow_tools/text_concatenator/text_concatenator_1/ text_concatenator_1 jobfile_text_concatenator_1.xml resultinfo_text_concatenator_1.xml
rm jobfile_text_concatenator_1.xml


for filepath in ./html/*
do
    printf "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" > ./jobfile_text_concatenator_1.xml
    printf "<text-concatenator-1-job>\n" >> ./jobfile_text_concatenator_1.xml
    printf "  <input>\n" >> ./jobfile_text_concatenator_1.xml
    printf "    <text-file path=\"./xhtml_pre.txt\"/>\n" >> ./jobfile_text_concatenator_1.xml
    printf "    <text-file path=\"%s\"/>\n" $filepath >> ./jobfile_text_concatenator_1.xml
    printf "    <text-file path=\"./xhtml_post.txt\"/>\n" >> ./jobfile_text_concatenator_1.xml
    printf "  </input>\n" >> ./jobfile_text_concatenator_1.xml
    printf "  <output-file path=\"%s.tmp\"/>\n" $filepath >> ./jobfile_text_concatenator_1.xml
    printf "</text-concatenator-1-job>\n" >> ./jobfile_text_concatenator_1.xml

    java -cp ./digital_publishing_workflow_tools/text_concatenator/text_concatenator_1/ text_concatenator_1 jobfile_text_concatenator_1.xml resultinfo_text_concatenator_1.xml

    rm jobfile_text_concatenator_1.xml
    rm $filepath
    mv $filepath.tmp $filepath
done


java -cp ./automated_digital_publishing/html2epub/html2epub2/ html2epub2 ./jobfile_html2epub2.xml
# TODO: epubcheck
cp ./epub/temp/out.epub ./epub/peeragogy_monthly_wrap_working_group_meeting_wraps.epub
rm -rf ./epub/temp/*

java -cp ./automated_digital_publishing/html2epub/html2epub2/ html2epub2 ./jobfile_html2epub2_20200506.xml
# TODO: epubcheck
cp ./epub/temp/out.epub ./epub/peeragogy_monthly_wrap_20200506.epub
rm -rf ./epub/temp/*

java -cp ./automated_digital_publishing/html2epub/html2epub2/ html2epub2 ./jobfile_html2epub2_20200520.xml
# TODO: epubcheck
cp ./epub/temp/out.epub ./epub/peeragogy_monthly_wrap_20200520.epub
rm -rf ./epub/temp/*

java -cp ./automated_digital_publishing/html2epub/html2epub2/ html2epub2 ./jobfile_html2epub2_20200527.xml
# TODO: epubcheck
cp ./epub/temp/out.epub ./epub/peeragogy_monthly_wrap_20200527.epub
rm -rf ./epub/temp/*

java -cp ./automated_digital_publishing/html2epub/html2epub2/ html2epub2 ./jobfile_html2epub2_20200804.xml
# TODO: epubcheck
cp ./epub/temp/out.epub ./epub/peeragogy_monthly_wrap_20200804.epub
rm -rf ./epub/temp/*

java -cp ./automated_digital_publishing/html2epub/html2epub2/ html2epub2 ./jobfile_html2epub2_20200824.xml
# TODO: epubcheck
cp ./epub/temp/out.epub ./epub/peeragogy_monthly_wrap_20200824.epub
rm -rf ./epub/temp/*


for filepath in ./html/*
do
    # Only file name.
    file=${filepath##*/}
    filename=${file%.*}

    printf "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" > ./jobfile_xhtml_to_latex_1.xml
    printf "<xhtml-to-latex-1-workflow-jobfile>\n" >> ./jobfile_xhtml_to_latex_1.xml
    printf "  <input-file path=\"./html/%s\"/>\n" $file >> ./jobfile_xhtml_to_latex_1.xml

    if [ "$filename" = "all" ];
    then
        printf "  <stylesheet-file path=\"./peeragogy_periodical_1.xsl\"/>\n" >> ./jobfile_xhtml_to_latex_1.xml
    else
        printf "  <stylesheet-file path=\"./peeragogy_periodical_2.xsl\"/>\n" >> ./jobfile_xhtml_to_latex_1.xml
    fi

    printf "  <output-file path=\"./latex_1/%s.tex\"/>\n" $filename >> ./jobfile_xhtml_to_latex_1.xml
    printf "</xhtml-to-latex-1-workflow-jobfile>\n" >> ./jobfile_xhtml_to_latex_1.xml

    java -cp ./digital_publishing_workflow_tools/workflows/xhtml_to_latex/xhtml_to_latex_1/ xhtml_to_latex_1 jobfile_xhtml_to_latex_1.xml resultinfo_xhtml_to_latex_1.xml

    rm jobfile_xhtml_to_latex_1.xml

    cd ./latex_1/

    xelatex $filename.tex
    xelatex $filename.tex
    makeindex $filename.idx
    xelatex $filename.tex
    xelatex $filename.tex

    cd ..
done

for filepath in ./html/*
do
    # Only file name.
    file=${filepath##*/}
    filename=${file%.*}

    printf "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" > ./jobfile_xhtml_to_latex_1.xml
    printf "<xhtml-to-latex-1-workflow-jobfile>\n" >> ./jobfile_xhtml_to_latex_1.xml
    printf "  <input-file path=\"./html/%s\"/>\n" $file >> ./jobfile_xhtml_to_latex_1.xml

    if [ "$filename" = "all" ];
    then
        printf "  <stylesheet-file path=\"./tufte_periodical_1.xsl\"/>\n" >> ./jobfile_xhtml_to_latex_1.xml
    else
        printf "  <stylesheet-file path=\"./tufte_periodical_2.xsl\"/>\n" >> ./jobfile_xhtml_to_latex_1.xml
    fi

    printf "  <output-file path=\"./latex_2/%s.tex\"/>\n" $filename >> ./jobfile_xhtml_to_latex_1.xml
    printf "</xhtml-to-latex-1-workflow-jobfile>\n" >> ./jobfile_xhtml_to_latex_1.xml

    java -cp ./digital_publishing_workflow_tools/workflows/xhtml_to_latex/xhtml_to_latex_1/ xhtml_to_latex_1 jobfile_xhtml_to_latex_1.xml resultinfo_xhtml_to_latex_1.xml

    rm jobfile_xhtml_to_latex_1.xml

    cd ./latex_2/

    xelatex $filename.tex
    xelatex $filename.tex
    makeindex $filename.idx
    xelatex $filename.tex
    xelatex $filename.tex

    cd ..
done
